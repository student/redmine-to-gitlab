# redmine-to-gitlab
Script to transfer redmine project to gitlab. 

Author: [Roman Denisov](mailto:roman@bisoft.fi)

License: [MIT](https://opensource.org/licenses/MIT)

### Features:
- saves issue numbering
- no dependencies other than python 3

### How to use:
- copy config file from sample

```shell
cp config.py.sample config.py
```
- modify config.py with your redmine and gitlab settings
- modify redmine-to-gitlab.py to suit your needs
- run 

```shell
python3 redmine-to-gitlab.py
```
