import inspect, json, collections, os

def uTable(rows, headers, html, **attributes):
 res = ""
 sep = "|"
 maxlen = [0] * (len(headers or rows[0]))
 for i, row in enumerate(rows):
  for j, item in enumerate(row):
   maxlen[j] = max(maxlen[j], len(str(item)))
  
  for j, item in enumerate(headers):
   maxlen[j] = max(maxlen[j], len(str(item)))
  line = sep.join("{: <" + str(maxlen[i]) + "}" for i, v in enumerate(headers))
 
 res += line.format(*headers) + '\n'
 
 def align(v):
  if type(v) in (int, float, complex):
   return ">"
  else:
   return "<"
 
 lines = []
 for row in rows:
  line = sep.join(["{: " + align(v) + str(maxlen[i]) + "}" for i, v in enumerate(row)])
  lines.append(line.format(*map(str, row)))
 
 if lines: res += '\n'.join(lines) + '\n'
 
 return res


def uExTable(ex):
 headers = "Function File Line Code".split()
 rows = []
 for frame, filename, lineno, function, code_context, index in inspect.trace():
  fn = os.path.basename(filename)
  context = ""
  if code_context: context = code_context.pop().replace("\n", " ")
  rows.append([function, fn, lineno, context])
 
 msg = 'Error: ' + str(ex)
 table = uTable(rows, headers, html=False)
 return '\n'.join([msg, table])


class AttrDict(collections.OrderedDict):
 def __getattr__(s, attr):
  return super().__getitem__(attr)
 
 def __setattr__(s, attr, value):
  super().__setitem__(attr, value)
 
 def __repr__(s):
  repr = json.dumps(s, indent=' ', ensure_ascii=False)
  return repr
