import http.client, urllib.request, json, email, logging, sys, collections, urllib.parse, time

from config import config
from util import AttrDict, uExTable

def gitlab(path, params=None, data=None, method='GET'):
 """http://docs.gitlab.com/ce/api/"""
 
 url = config['gitlab url']+path

 if params: url += '?'+urllib.parse.urlencode(params)
 if data: data = json.dumps(data).encode()

 req = urllib.request.Request(url, data=data, headers={'PRIVATE-TOKEN': config['gitlab token'], 'Content-Type': 'application/json'}, method=method)
 res = urllib.request.urlopen(req)
 
 if res.getcode() == 201:  # 201 Created. The POST request was successful and the resource is returned as JSON.
  pass
 elif res.getcode() != 200:
  raise Exception('bad status code {} {} for url {}'.format(res.getcode(), res.msg, url))

 j = json.loads(res.read().decode(), object_hook=AttrDict)
 return j

def redmine(path, params=None, data=None, method='GET'):
 """http://www.redmine.org/projects/redmine/wiki/Rest_api"""
 
 url = config['redmine url']+path
 
 if params: url += '?'+urllib.parse.urlencode(params)
 if data: data = json.dumps(data).encode()
   
 req = urllib.request.Request(url, data=data, headers={'X-Redmine-API-Key': config['redmine token'], 'Content-Type': 'application/json'})
 res = urllib.request.urlopen(req)
 if res.getcode() != 200:
  raise Exception('bad status code {} {} for url {}'.format(res.getcode(), res.msg, url))

 j = json.loads(res.read().decode(), object_hook=AttrDict)
 return j


def main():
 
 
 # get redmine project info
 redmine_projects = redmine('/projects.json')
 redmine_project = next(p for p in redmine_projects.projects if p.identifier == config['redmine project'])
 logging.info('redmine project id {} {}'.format(redmine_project.id, redmine_project.name))
 
 # get gitlab project info
 gitlab_projects = gitlab('/projects/owned')
 gitlab_project = next((p for p in gitlab_projects if p.name == config['gitlab project']), None)
 
 # # delete gitlab project if it exists!
 # if gitlab_project:
 #  logging.info('deleting gitlab project {} {}'.format(gitlab_project.name, gitlab_project.id))
 #  gitlab('/projects/{}'.format(gitlab_project.id), method='DELETE')
 #  time.sleep(30)
 #  gitlab_project = gitlab('/projects', params=dict(name=config['gitlab project']), method='POST')

 logging.info('gitlab project {} {}'.format(gitlab_project.id, gitlab_project.name))
 
 # get gitlab user info
 gitlab_user = gitlab('/users?username='+config['gitlab username'])[0]
 
 # download issues list from redmine
 offset = 0
 limit_max = 100
 redmine_issues = []
 while True:
  redmine_issues_page = redmine('/issues.json', params=dict(project_id=redmine_project.id, offset=offset, limit=limit_max, status_id='*'))
  redmine_issues.extend(redmine_issues_page.issues)
  
  if len(redmine_issues_page.issues) == limit_max:
   offset += limit_max
  else:
   break
   
 logging.info('redmine, {} issues listed'.format(len(redmine_issues)))
 
 # sort issues, because gitlab does not allow issue id update
 redmine_issues.sort(key=lambda v: v.id)
 
 # starting from last created issue
 min_id = 1
 gitlab_issues = gitlab('/projects/{}/issues'.format(gitlab_project.id))
 if gitlab_issues:
  min_id = gitlab_issues[0].iid + 1
  logging.info('continuing from issue {}'.format(min_id))
 max_id = redmine_issues[-1].id
 for ri_id in range(min_id, max_id+1):
  redmine_issue = next((ri for ri in redmine_issues if ri.id == ri_id), None)
  if redmine_issue:
   
   # convert redmine issue to gitlab issue
   logging.info('redmine issue {}\n'.format(ri_id)+str(redmine_issue))
   
   gitlab_issue = AttrDict()
   gitlab_issue.id = gitlab_project.id
   gitlab_issue.title = redmine_issue.subject
   gitlab_issue.description = redmine_issue.description
   gitlab_issue.confidential = False
   gitlab_issue.assignee_id = gitlab_user.id
   gitlab_issue.milestone_id = None
   gitlab_issue.labels = 'redmine'
   gitlab_issue.created_at = redmine_issue.created_on
   gitlab_issue.due_date = redmine_issue.get('due_date', None)

  else:
   # create temporary issue to make sure gitlab matches redmine issue id number
   gitlab_issue = AttrDict()
   gitlab_issue.id = gitlab_project.id
   gitlab_issue.title = 'to be deleted issue {}'.format(ri_id)
  
  # create gitlab issue
  gi = gitlab('/projects/{}/issues'.format(gitlab_project.id), data=gitlab_issue, method='POST')
  
  if redmine_issue and redmine_issue.id != gi.iid:
   raise Exception('redmine issue id {} does not match gitlab issue id {}'.format(redmine_issue.id, gi.iid))

  logging.info('gitlab issue {} created\n'.format(gi.iid)+str(gi))
  
  # set gitlab issue closed status
  if redmine_issue and redmine_issue.status.name == 'Closed':
   gitlab('/projects/{}/issues/{}'.format(gi.project_id, gi.id), params=dict(state_event='close'), method='PUT')
   
  if not redmine_issue:
   # delete temporary issue that we had to create before
   gitlab('/projects/{}/issues/{}'.format(gi.project_id, gi.id), method='DELETE')
   
  # transfer comments
  if redmine_issue:
   redmine_issue = redmine('/issues/{}.json'.format(redmine_issue.id), params=dict(include='children,attachments,relations,changeset,journals')).issue
   
   # making first comment with json of redmine issue
   body = '```json\n'+json.dumps(redmine_issue, ensure_ascii=False)+'\n```'
   gitlab('/projects/{}/issues/{}/notes'.format(gi.project_id, gi.id), data=dict(id=gi.project_id, issue_id=gi.id, body=body, created_at=gi.created_at[:len('2007-01-01T05:21:00')]+'Z'), method='POST')
   
   
   if 'journals' in redmine_issue:
    for j in redmine_issue.journals:
     if 'notes' in j and j.notes:
      c = gitlab('/projects/{}/issues/{}/notes'.format(gi.project_id, gi.id), data=dict(id=gi.project_id, issue_id=gi.id, body=j.notes, created_at=j.created_on[:len('2007-01-01T05:21:00')]+'Z'), method='POST')
      logging.info('comment:\n'+str(c))
      
 
if __name__ == '__main__':
 logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='%(asctime)s %(levelname)s(%(filename)s:%(lineno)d)\t%(message)s')
 
 try:
  main()
 except Exception as ex:
  logging.error(uExTable(ex))
  sys.exit(1)
